<?php
include "php/header.php";
// if user's society is not actiavted, redirect
if($_SESSION['socStatus']==-1) { header("location:signup-org.php"); }
// if user's society is waiting approval or suspended, redirect
if($_SESSION['socStatus']==2 or $_SESSION['socStatus']==0) { header("location:suspend.php"); }

$errors = array();
if(isset($_POST['signup_evt_submit'])){
	require "php/createEventDB.php";
	unset($_POST['signup_evt_submit']);
}
?>
<script>
function noPic() {
	if(document.forms[0]["fileToUpload"].value=="")
		if(!confirm("Are you sure not to upload an event picture?"))
			return false;
	return true;
}
</script>
<!-- Photobox banner -->
<div id="signuporg_photobox" class="container container-white">
	<div id="floatingBlueBox" class="container-translucent-blue">
		<img src="img/support_org.png"><br>
		Create<br>Event
	</div>
</div>

<div class="container container-white">
<b><?php include "php/errors.php"; ?></b>
<form action="createEvent.php" onsubmit="return noPic();" method="post" enctype="multipart/form-data">
	<table class="signup_org_form">
		<tr><td>*Mandatory Field<br><br><br><br></td></tr>
		<tr>
			<td>*Event Name</td>
			<td><input name="name" type="text" value="" class="text_input" required></td>
		</tr>
			<tr>
				<td>
					Event Logo<br><div class="form_remarks">(File size less than 500KB)</div>
				</td>
				<td>
					 <input type="file" name="fileToUpload" accept="image/*">
				</td>
			</tr>
			<tr>
				<td>*Event Date<br><div class="form_remarks">(yyyy-mm-dd)</div></td>
				<td><input name="date" type="text" value="" class="text_input" pattern="^\d{4}-\d{2}-\d{2}$" required></td>
			</tr>
		<tr>
			<td>
				*Event Description<br><div class="form_remarks">(Maximum length: 2000)</div>
			</td>
			<td>
				<textarea name="intro" class="text_input_box" required></textarea>
			</td>
		</tr>
		<tr></tr>
		<tr><td><button type="submit" id="signup_evt_submit" name="signup_evt_submit">SUBMIT</button></td></tr>
	</table>
</form>
</div>

<?php include "php/footer.php" ?>
