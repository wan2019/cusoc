<?php include "php/header.php" ;
$showPastEvents = false;

if(isset($_GET["PastEvents"]) == true){
	$showPastEvents = true;
}
?>
<!-- Photobox browse organisations -->
<div id="browse_event_photobox" class="container container-white">
	<div id="floatingBlueBox" class="container-translucent-blue">
		<img src="img/upcoming_event_icon.png"><br>
		Browse<br>Events
	</div>
</div>
<div id="event_complete_search" class="container-orange"><center>
<form>

<table>
<tr>
	<td>
	<td><i class="fas fa-search" id="org_search_icon"></i>
	<input id="org_search" onkeyup="myFunction()" name="search_word" type="text" value=""></td>
</tr>
</table><?php
if(isset($_GET["PastEvents"]) == true){

echo '<a href="http://localhost/event.php">Show future Events</a>';
}
else{
	echo '<a href="http://localhost/event.php?PastEvents=da">Show all Events</a>';
}
?>

</form>
</div>
<div class="container" id="socData">
<?php //require "php/eventDB.php";
	require_once("php/connDB.php");
	if($showPastEvents == true){
		// https://www.w3schools.com/php/php_mysql_select.asp
		$sql = "SELECT * FROM event";
		$result = $conn->query($sql);


		//echo($result->num_rows);
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
			echo'
			<div  class="container-long-orange">
			<table><tr>
			<td><a href="event-details.php?id='.$row['id'].'"><img src="upload/eventPic/'.$row["img"].'"/></a></td>
			<td><div class="text-container"><a><div class="org_name">'.$row["Name"].'</div></a><br>'.$row["Description"].'
			</div></td></tr></table></div>';
			}
		} else {
			echo "0 results";
		}
		$conn->close();
	}else{
		// https://www.w3schools.com/php/php_mysql_select.asp
		$time = time(); //time stored as POSIX time in database
		$sql = "SELECT * FROM event WHERE `eventDate` > ".$time; //Filter for future events only
		//echo 'Time is ' .$time . '<br>';
		$result = $conn->query($sql);


		//echo($result->num_rows);
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
			echo'
			<div  class="container-long-orange">
			<table><tr>
			<td><a href="event-details.php?id='.$row['id'].'"><img src="upload/eventPic/'.$row["img"].'"/></a></td>
			<td><div class="text-container"><a><div class="org_name">'.$row["Name"].'</div></a><br>'.$row["Description"].'
			</div></td></tr></table></div>';
			}
		} else {
			echo "0 results";
		}
		$conn->close();
	}


?>

</div>
<script src="js/eventDB.js"></script>
<?php include "php/footer.php" ?>
