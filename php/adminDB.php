<?php
// called by admin.php

// display content according to the option
if(!isset($_GET['action'])) {
  $_GET['action']=1;
}
switch ($_GET['action']) {
	case 1: default:
		$button="Activate";
    $link="org-details.php?soc=";
    $sql = "SELECT id, Name, Description, img FROM soc WHERE `soc`.`status` = 0 OR `soc`.`status` = 2";
    $dir = "upload/socLogo/";
    $_GET['action']=1;
		break;
	case 2:
		$button="Suspend";
		$link="org-details.php?soc=";
    $sql = "SELECT id, Name, Description, img FROM soc WHERE `soc`.`status` = 1";
    $dir = "upload/socLogo/";
		break;
	case 3:
		$button="Delete";
		$link="event-details.php?event=";
    $sql = "SELECT id, Name, Description, img FROM event";
    $dir = "upload/eventPic/";
		break;
	case 4:
		$button="Delete";
		$link="org-details.php?soc=";
    $sql = "SELECT id, Name, Description, img FROM soc";
    $dir = "upload/socLogo/";
    break;
}

// display society or event list
require_once("connDB.php");
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
      echo'
      <div class="container-long-orange" style="height:100px">
      <table>
        <tr>
          <td>
            <a href='.$link.$row["id"].'">
              <img style="width:100px;height:100px" src="'.$dir.$row["img"].'"/>
            </a>
          </td>
          <td>
            <div class="text-container">
              <a>
                <div class="entryName">
                  '.$row["Name"].'
                </div>
              </a>
              <br>
              '.$row["Description"].'
            </div>
          </td>
          <td style="padding-right: 2%">
            <button type="submit" onclick="popUp('.$_GET['action'].',this.id,this.name)" id="'.$row["id"].'" name="'.$row["Name"].'">'.$button.'</button>
          </td>
        </tr></table></div>';
    }
} else {
    echo "0 results";
}
$conn->close();
?>
