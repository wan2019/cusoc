<?php
// used by createEvent.php to process a submitted form
// simplified from signupOrgDB.php

if (isset($_POST['signup_evt_submit'])) {
  require "php/connDB.php";
  $name = mysqli_real_escape_string($conn, $_POST['name']);
  $intro = mysqli_real_escape_string($conn, $_POST['intro']);
  $date = mysqli_real_escape_string($conn, $_POST['date']);
  if (mysqli_fetch_assoc(mysqli_query($conn, "SELECT Name FROM event WHERE Name='$name' LIMIT 1"))) {
    array_push($errors, "Event with same name already exists");
  }
  if (strlen($date) != 10) {
    array_push($errors, "Event date is in wrong format.");
  }
  if (strlen($intro)>2000) {
    array_push($errors, "Description is too long.");
  }
  if (count($errors) == 0) {
    // try to upload image if there is
    if(!$_FILES['fileToUpload']["name"]==""){
      $target_dir = "upload/eventPic/";
      require "uploadImage.php";
      $img = $_FILES['fileToUpload']["name"];
    } else{
      $img = "cuhk.jpg";
    }
    if (count($errors) == 0) {
      $username=$_SESSION['username'];
      $date=strtotime($date);
      $socID=mysqli_fetch_assoc(mysqli_query($conn, "SELECT socID FROM user WHERE username='$username' LIMIT 1"))['socID'];
      $conn->query("INSERT INTO event (Name, eventDate, Description, img, socID) VALUES ('$name', '$date', '$intro','$img','$socID')");
      array_push($errors, 'Event "'.$name.'" has been uploaded successfully.');
    }
  }
  $conn->close();
}
?>
