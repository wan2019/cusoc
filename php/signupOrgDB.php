<?php
// used by signup-org.php to process a submitted form

if (isset($_POST['signup_org_submit'])) {
  require "php/connDB.php";
  $name = mysqli_real_escape_string($conn, $_POST['name']);
  $intro = mysqli_real_escape_string($conn, $_POST['intro']);
  if (mysqli_fetch_assoc(mysqli_query($conn, "SELECT Name FROM soc WHERE Name='$name' LIMIT 1"))) {
    array_push($errors, "Society with same name already exists");
  }
  if (strlen($intro)>2000) {
    array_push($errors, "Introduction is too long.");
  }
  if (count($errors) == 0) {
    // try to upload image if there is
    if(!$_FILES['fileToUpload']["name"]==""){
      $target_dir = "upload/socLogo/";
      require "uploadImage.php";
      $img = $_FILES['fileToUpload']["name"];
    } else{
      $img = "cuhk.jpg";
    }
    if (count($errors) == 0) {
      $type=$_POST["type"];
      $nature=$_POST["nature"];
      $username=$_SESSION['username'];
      $userID=mysqli_fetch_assoc(mysqli_query($conn, "SELECT id FROM user WHERE username='$username' LIMIT 1"))['id'];
      $conn->query("INSERT INTO soc (Name, type, nature, Description, img, userID) VALUES ('$name','$type','$nature','$intro','$img','$userID')");
      $socID=mysqli_fetch_assoc(mysqli_query($conn, "SELECT id FROM soc WHERE Name='$name' LIMIT 1"))['id'];
      $conn->query("UPDATE `user` SET `socID` = ".$socID." WHERE `user`.`id` = ".$userID);
      $conn->close();
      $_SESSION['socStatus']=2;
      $conn->close();
      header("location:suspend.php");
    }
  }
  $conn->close();
}
?>
