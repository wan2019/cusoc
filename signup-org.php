<?php
include "php/header.php";
// if user's society is actiavted, redirect to create event page
if($_SESSION['socStatus']==1) { header("location:createEvent.php"); }
// if user's society is waiting approval, redirect as well
if($_SESSION['socStatus']==2 or $_SESSION['socStatus']==0) { header("location:suspend.php"); }
// if this form is submitted, process it
$errors = array();
if(isset($_POST['signup_org_submit'])){
	require "php/signupOrgDB.php";
	unset($_POST['signup_org_submit']);
}
?>
<script>
function noPic() {
	if(document.forms[0]["fileToUpload"].value=="")
		if(!confirm("Are you sure not to upload a logo?"))
			return false;
	return true;
}
</script>
<!-- Photobox banner -->
<div id="signuporg_photobox" class="container container-white">
	<div id="floatingBlueBox" class="container-translucent-blue">
		<img src="img/support_org.png"><br>
		Create<br>Organisation<br>Account
	</div>
</div>

<div class="container container-white">
<b><?php include "php/errors.php"; ?></b>
<form action="signup-org.php" onsubmit="return noPic();" method="post" enctype="multipart/form-data">
	<table class="signup_org_form">
		<tr><td>*Mandatory Field<br><br><br><br></td></tr>
		<tr>
			<td>*Organisation Name</td>
			<td><input name="name" type="text" value="" class="text_input" required></td>
		</tr>
		<tr>
			<td>*Type of Group</td>
			<td>
				<input type="radio" name="type" value="1" checked> Organisation
				<input type="radio" name="type" value="2"> Interest Group
			</td>
		</tr>
		<tr>
			<td>*Event Nature<br></td>
			<td>
				<select id="org_type_dropdown" name="nature" required>
					<option value="">Please select</option>
					<option value="1">Academics</option>
					<option value="2">Performance</option>
					<option value="3">Business</option>
					<option value="4">Recreational</option>
					<option value="5">Language</option>
					<option value="6">Public Service</option>
					<option value="7">Sports</option>
					<option value="8">Religious</option>
					<option value="9">Student Union</option>
				</select>
			</td></tr><tr></tr>
			<tr>
				<td>
					Organisation Logo<br><div class="form_remarks">(File size less than 500KB)</div>
				</td>
				<td>
					 <input type="file" name="fileToUpload" accept="image/*">
				</td>
			</tr>
		<tr>
			<td>
				*Organisation Introduction<br><div class="form_remarks">(Maximum length: 2000)</div>
			</td>
			<td>
				 <input name="intro" type="text" value="" class="text_input_box" required>
			</td>
		</tr>
		<tr></tr>
		<tr><td><button type="submit" id="signup_org_submit" name="signup_org_submit">SUBMIT</button></td></tr>
	</table>
</form>
</div>
<?php include "php/footer.php" ?>
