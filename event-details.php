<?php include "php/header.php" ?>
<?php

require_once("php/connDB.php");
$result = $conn->query("SELECT * FROM `event` WHERE `id` = ".$_GET['id']);
$row = $result->fetch_assoc();

$conn->close();
?>
<div class="container container-white">

<div class="path-text-container">

	<a href="event.php">Events  </a><i class="fas fa-chevron-right"></i> <?php echo $row["Name"] ?>

</div>

<!--org info TODO contact and favourite buttons-->
	<div id="socWSDC" class="org-page-details">
		<div class="container.white">
        <h1>
            <div class="org_name"><?php echo $row["Name"] ?>
            </div>
        </h1>
			<table id="event_info_table">
				<tr>
					<td id="key_image_cell">
						<img id="org_key_image" src="upload/eventPic/<?php echo $row["img"] ?>">
					</td>
					<td>
						<div class="text-container">

							</div>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<p id="org_details_text"><?php echo $row["Description"] ?>	</p>


</div>
<?php include "php/footer.php" ?>
