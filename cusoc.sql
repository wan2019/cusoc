-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 04, 2019 at 04:26 PM
-- Server version: 5.7.24
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cusoc`
--

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
CREATE TABLE IF NOT EXISTS `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `eventDate` int(11) NOT NULL,
  `Description` varchar(2000) NOT NULL,
  `img` varchar(100) NOT NULL,
  `socID` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `Name`, `eventDate`, `Description`, `img`, `socID`) VALUES
(2, 'Lion Rock Hiking Trip', 0, 'April 23, Tuesday\r\n8:30-15:00 Country Park(Won Tai Sin) to Lion Rock Peak\r\nFree of Charge!\r\n#InternationalStudentsAssociation\r\n\r\nLooking for something adventurous to do in reading week? Well this is your chance! Join us as we hike together along this iconic peak and witness its breathtaking views of the city, while enjoying the great company of one another as one ISA family!', 'hiking.isa.jpg', 2),
(3, 'Inauguration Ceremony', 0, 'April 24, Wednesday\r\n19:00-21:00 LSK LT3\r\nCome Stop By!\r\n#StrategicMarketingSociety. Its our ceremony. Please come!', 'inaug.strategicmarketing.jpg', 3),
(4, 'Women\'s Rowing Team Tryouts', 0, 'April 25, Thursday\r\n10:00-12:00 Shek Mun Rowing Centre\r\n#CUHKRowingClub Selection for college team will begin this week! Even if you still haven’t had a chance to come to our fun days or trainings but would still like to try out, you are more than welcome to join us. For those who are ready please come to our selection sessions ', 'rowingteamtryouts.rowingclub.jpg', 4),
(5, 'Inter-U Softball Tournament', 0, 'April 26, Friday\r\nAll-day HKUST\r\n#CUHKSoftballClub Come show your CUHK spirit by cheering for our university softball team as they go against HKUST, HKU, and BU!\r\n', 'softball.jpg', 5),
(6, 'LGBT Parade', 0, 'April 27, Saturday\r\n12:00-14:00 Queen\'s Road, Central\r\n#AIESEC Come learn more about LGBT views and opions, and participate in a parade to show your support! Dress-code: RAINBOW!', 'aiesec.png', 6),
(9, 'fre', 0, 'fer', 'cuhk.jpg', 2),
(10, 'a', 0, 'a', 'cuhk.jpg', 3),
(11, 'ff', 0, 'dddddddddddd', 'cuhk.jpg', 3),
(12, 'dew3qdw', 1557000000, 'dwef2q', 'cuhk.jpg', 3),
(13, 'aaaa', 631152000, 'aaa', 'cuhk.jpg', 3),
(14, 'zzz', 1556928000, 'zz', 'taobao.gif', 3);

-- --------------------------------------------------------

--
-- Table structure for table `soc`
--

DROP TABLE IF EXISTS `soc`;
CREATE TABLE IF NOT EXISTS `soc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `nature` tinyint(4) NOT NULL,
  `Description` varchar(2000) NOT NULL,
  `img` varchar(100) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '2',
  `userID` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soc`
--

INSERT INTO `soc` (`id`, `Name`, `type`, `nature`, `Description`, `img`, `status`, `userID`) VALUES
(2, 'The Chinese Martial Arts Society of United College', 2, 2, 'The Chinese Martial Arts Society of United College was founded in 1987. Over the 30 years, we have been striving to promote the North Shaolin Kung Fu among students and alumni of CUHK. The charm of Chinese Kung Fu has brought together local, mainland and exchange students, as well as many long graduated alumni.', 'phoenixWaterPoloClub.jpg', 1, 11),
(3, 'Chung Chi The Suns', 1, 3, 'Chung Chi The Suns has been collaborating with primary schools in Qingyuan, Wuzhou, Mianyang and many other places. We have given voluntary teaching service to the local students as well as assisted in simple renocation work. As the name of our organization suggests, we strongly believe that although the sly might be covered in thick layers of clouds, the warm and bright sun above is always present with us. We would like to share this belief with those we serve.', 'ccthesuns.png', 1, 14),
(4, 'Chung Chi Drama Club', 1, 4, 'Promoting and spreading the joyful drama atmosphere in both the CUHK and Chung Chi College, plus hoping to popularize the drama culture.', 'ccdramaclub.jpg', 0, 15),
(5, 'CUHK Dargon Boat Club', 2, 5, 'Being an International student, are you ever interested about the Chinese culture? Here\'s a fabulous chance! Don\'t hesitate and join CUHK Dragon Boat Club now! For more details, please visit our Facebook Page: CUHK Dragon Boat Club.', 'dragonboat.jpg', 0, 0),
(6, 'Doujin Culture Society', 1, 6, 'Doujin is a Japanese term that is similar to fandom. It refers to a group of people who share common interests, hobbies, or achievement.\r\n\r\nOur aim is to gather these people and provide a platform for them to share their self-created fanworks, including mangas, fictions, fan guides, art collections, music and videos.', 'doujin.PNG', 0, 0),
(7, 'Custore', 0, 0, 'Supporting local production and encouraging interaction between producers and consumers. Reducing schoolmates\' reliance on oligopolists by providing other options. Promoting conscientious consumerism.', 'phoenixWaterPoloClub.jpg', 0, 0),
(8, 'CUHK English Debate Team', 0, 0, '\r\nAs the only English Language InterVarsity debate team in the university, we are a competitive parliamentary debate team and regularly participate in regional and international debate tournaments across the world, from Tokyo to Toronto. As we debate, we also experience societies, cultures, and thoughts both mainstream and niched. Our team is considered to be one of the best in Northeast Asia. For many on the varsity team, debating is the defining feature of their college years.', 'englishdebate.jpg', 0, 0),
(9, 'Game and Chess Society', 0, 0, '\r\nGame and Chess Society is dedicated to promote chess and board game culture on campus. We give CUHK students possibilities to broaden their horizon about chess and board game.', 'gameAndChess.jpg', 1, 0),
(10, 'The Golden Z Club', 0, 0, 'The CUHK Golden Z Club is one of Zonta International\'s longest-running program, which helps university students develop leadership skills, promote career exploration and encourage members to participate in community, school and international service projects.', 'theGoldenZ.jpg', 0, 0),
(12, 'Modern Dance Society', 0, 0, 'The Chinese University of Hong Kong Modern Dance Society organizes three major yearly events, including: Joint University Mass Dance, Intra-Varsity Dance Competition (ivdc), and Annual Performance. Through these events, our society hopes to provide you with the opportunity to partake in various dance types, such as street and modern dance, while simultaneously promoting dance culture in Hong Kong.', 'modernDance.jpg', 0, 0),
(13, 'Sailing Club', 0, 0, 'The Sailing Club is established with a purpose to share our love of sailing and the freedom of going to where the wind leads. We hold yacht sailing event every month and dinghy sailing course from time to time, so you can have a taste of every aspect of sailing.', 'sailingclub.jpg', 0, 0),
(14, 'ReadWhite', 0, 0, 'Through publishing a bi-monthly magazine, we hope to share our thoughts with our readers and reflect on university school life of CUHK students.', 'ReadWhite.jpg', 0, 0),
(15, 'Phoenix Water Polo Club', 0, 0, 'We introduce the sport of water polo to students and hope to gather those who enjoy fitness and water polo to our on campus tranings and competitions.', 'phoenixWaterPoloClub.jpg', 0, 0),
(16, 'New Asia Kung Fu Society', 0, 0, 'The New Asia Kung Fu Society was established in 1979. Our head coach Master Joe Kwong, teaches Chinese martial arts (Choy Lee Fut style), lion dancing and associated instruments. We practise every Monday and Thursday evening even on rainy days and during summer holiday. We welcome any students or staff to join our Kung Fu family, no matter you are experienced or not.', 'newAsiaKungFu.png', 0, 0),
(17, 'The Mahjong Study Society', 0, 0, 'Our society aims to increase students\' knowledge towards Mahjong. We encourage students to study various Mahjong rules in different countries and districts, and learn how to play Mahjong referring to Statistics and Probability theories.', 'mahjongStudySociety.jpg', 0, 0),
(18, 'The Wine Society', 0, 0, 'The Wine Society of the Chinese University of Hong Kong is the first wine society at university level in Hong Kong, foundd by a group of wine lovers in 2010 with the vision to promote win appreciation culture within CUHK.', 'wineso.png', 0, 0),
(19, 'Three Heart Club', 0, 0, 'Three heart club is always devoted to offering help to and communicating with mainland primary school and middle school students, through interesting lectures, tutorials, and group games. The society consists of executive committee members(ECM) and volunteers.', 'threehearts.jpg', 0, 0),
(20, 'S.H.Ho Ving Tsun Society', 0, 0, 'We aim to promote Chinese martial arts, allowing our members a chance to experience and learn traditional martional arts. Also we wish to promote a sporty lifestyle, enhance our physique and provide our members a chance to learn self-defence.', 'vingtsun.jpg', 0, 0),
(21, 'Shaw Band', 0, 0, 'Shaw Band is the first established band club in CU. Our purpose is to promote local indie band sound, spread indie band culture in CU. We organize instrument classes, hold indie band shows both within CU and in the live house. Also, Shaw Band provides a platform for you to meet new band lovers, or even form your own band!', 'shawband.jpg', 0, 0),
(22, 'Strategic Marketing Society', 0, 0, 'Strategic Marketing Society (SMS) is a non-profit making student run organization of the Chinese University of Hong Kong. Founded in 2005 and as the sole marketing based organization in CUHK, SMS dedicates its efforts to organizing diverse activities to business students, with the aim of implanting fundamental marketing knowledge and enhancing marketing skills to business students, especially our members.', 'strategicmark.jpg', 0, 0),
(23, 'Softball Club', 0, 0, 'Our club is dedicated to promoting softball, a lesser known sport to newcomers in CU. Through participating in consistent practises, our members get to be confident, disciplined and strong-minded as a team. Our focus is on providing a positive experience for all our players with teh ultimate goal of providing our players with a well-organised and safe means of competing and having fun.', 'softball.jpg', 0, 0),
(24, 'CUHK Rowing Club', 0, 0, 'The Chinese University of Hong Kong Rowing Team was founded in 1986, and so the Chinese University Rowing Club followed and was formed in the same year. The purpose of our society is to promote the sport of rowing, to allow those even without previous experience to gain exposure to this sport.', 'rowing.jpg', 0, 0),
(25, 'CUHK Rock Climbing Club', 0, 0, 'Our society is aiming to promote Rock Climbing and Sport Climbing among CUHK students as well as selecting outstanding climbers to train and compete locally or overseas, representing CUHK.', 'rockclimbing.jpg', 0, 0),
(26, 'AIESEC in CUHK', 0, 0, 'AIESEC is a global youth-led organization striving to achieve peace and fulfillment of humankind\'s potential by activating leadership qualities in youth through learning from practical experiences in challenging environments.', 'aiesec.png', 0, 0),
(27, 'International Students Association CUHK', 0, 0, 'We are the International Students Association, a family for students of all colors from diverse backgrounds, here in the Chinese University of Hong Kong.', 'isa.jpg', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `socID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `email`, `socID`) VALUES
(12, 'admin', '0cc175b9c0f1b6a831c399e269772661', '', 0),
(11, 'a', '0cc175b9c0f1b6a831c399e269772661', '1155077307@link.cuhk.edu.hk', 0),
(16, 'abc', '900150983cd24fb0d6963f7d28e17f72', '1155064104@link.cuhk.edu.hk', 2),
(13, 'bb', '0cc175b9c0f1b6a831c399e269772661', '', 1),
(14, 'cc', '0cc175b9c0f1b6a831c399e269772661', '', 3),
(15, 'dd', '0cc175b9c0f1b6a831c399e269772661', '', 0),
(17, 'ee', '0cc175b9c0f1b6a831c399e269772661', '', 0),
(19, 'csci3100', '74b87337454200d4d33f80c4663dc5e5', 'wylat@link.cuhk.edu.hk', 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
