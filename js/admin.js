// called by admin.php

function popUp(action,id,name) {
  var verb = ["Activate","Suspend","Delete","Delete"];
  if (confirm("Are you sure to "+verb[action-1]+" "+name+"?")) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", "php/admin_action.php?action="+action+"&id="+id, true);
    xmlhttp.send();
    location.reload(true);
  }
}
// https://www.w3schools.com/howto/howto_js_filter_table.asp
function myFunction() {
  // Declare variables
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("searchBar");
  filter = input.value.toUpperCase();
  table = document.getElementById("readData");
  tr = table.getElementsByClassName("container-long-orange");

      // Loop through all table rows, and hide those who don't match the search query
  for (i = 0; i < tr.length; i++) {
    if (input.value=="") {
      tr[i].style.display = "";
      continue;
    }
    td = tr[i].getElementsByClassName("entryName")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}
