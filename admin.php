<?php
include "php/header.php";
// non-Administrator is not allowed to visit this page
if($_SESSION['username']!="admin") { header('location: home.php'); }
?>

<center>
<h4><b>
Administrator Console -
<select onchange="location=this.value;">
	<option>Please select</option>
	<option value="admin.php?action=1">Activate Society</option>
	<option value="admin.php?action=2">Suspend Society</option>
	<option value="admin.php?action=3">Delete Event</option>
	<option value="admin.php?action=4">Delete Society</option>
</select>
</b></h4>
<form>
<table><tr><td>
	<i class="fas fa-search" id="org_search_icon"></i>
	<input id="searchBar" onkeyup="myFunction()" name="search_word" type="text" value="">
</td></tr></table>
</form>
</center>

<div class="container" id="readData">
<?php	require "php/adminDB.php"; ?>
</div>
<script src="js/admin.js"></script>
<script>
if(navigator.userAgent.indexOf("Firefox") != -1){
	alert("Administrator function is currently unavailable in Firefox. Please use Chrome, IE, Edge instead.");
}
</script>
<?php include "php/footer.php" ?>
